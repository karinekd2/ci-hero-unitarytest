package com.ci.hero.model;

import com.ci.hero.common.Const;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class HeroTest {

    protected Hero heroTest;

    @Before
    public void setUp() {
        heroTest = new Hero("testHero");
    }

    @After
    public void tearDown() {
    }

    @Test
    public void attackValueMinMaxTest(){
        heroTest.setAttack(10);
        heroTest.setAttackStd(1);

        int sum_below_6=0;
        int sum_upper_12=0;
        for(int i=0;i<1000;i++){
                float attackValue=this.heroTest.attack();
                if(attackValue<=6){
                    sum_below_6++;
                }
            if(14<=attackValue) {
                sum_upper_12++;
            }
        }

        System.out.println("sum_below_6:"+sum_below_6+", sum_upper_12:"+sum_upper_12);
        assertTrue(sum_below_6<=1);
        assertTrue(sum_upper_12<=1);
    }

    @Test
    public void attackValueAvgTest(){
        heroTest.setAttack(10);
        heroTest.setAttackStd(1.0f);
        int nb_try=100;
        int sum_in_90=0;
        int sum_out_90=0;
        for(int i=0;i<nb_try;i++){
            float attackValue=this.heroTest.attack();
            if(9<=attackValue && attackValue<=11){
                sum_in_90++;
            }else{
                sum_out_90++;
                System.out.println("outside"+(attackValue));
            }
        }
        System.out.println("expected 90:"+(nb_try*90/100));
        System.out.println("sum_in_90:"+sum_in_90+", sum_out_90:"+sum_out_90);
        assertTrue(sum_in_90>=(nb_try*90/100));

    }


    @Test
    public void checkHpBoundTest(){
        this.heroTest.setHp(Const.HP_BOUND_MAX+20);
        assertTrue(this.heroTest.getHp()<=Const.HP_BOUND_MAX);

        this.heroTest.setHp(Const.HP_BOUND_MIN-1);
        assertTrue(this.heroTest.getHp()>=Const.HP_BOUND_MIN);
    }

    @Test
    public void checkEnergyBoundTest(){
        this.heroTest.setEnergy(Const.ENERGY_BOUND_MAX+20);
        assertTrue(this.heroTest.getEnergy()<=Const.ENERGY_BOUND_MAX);

        this.heroTest.setEnergy(Const.ENERGY_BOUND_MIN-1);
        assertTrue(this.heroTest.getEnergy()<=Const.ENERGY_BOUND_MIN);
    }

    @Test
    public void checkAttackBoundTest(){
        this.heroTest.setAttack(Const.ATTACK_BOUND_MAX+20);
        assertTrue(this.heroTest.getAttack()<=Const.ATTACK_BOUND_MAX);

        this.heroTest.setAttack(Const.ATTACK_BOUND_MIN-1);
        assertTrue(this.heroTest.getAttack()>=Const.ATTACK_BOUND_MIN);

    }

    @Test
    public void checkDefenseBoundTest(){
        this.heroTest.setDefense(Const.DEFENSE_BOUND_MAX+20);
        assertTrue(this.heroTest.getDefense()==Const.DEFENSE_BOUND_MAX);

        this.heroTest.setDefense(Const.DEFENSE_BOUND_MIN-1);
        assertTrue(this.heroTest.getDefense()>=Const.DEFENSE_BOUND_MIN);
    }


}
