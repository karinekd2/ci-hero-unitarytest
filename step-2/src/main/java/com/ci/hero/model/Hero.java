package com.ci.hero.model;

import com.ci.hero.common.Const;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsonable;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Random;

public class Hero implements Jsonable {
    private Random rand;
    private String name;
    private String superPower;
    private String elementalAffinity;
    private String elementalVulnerability;
    private int hp;
    private int energy;
    private int attack;
    private float attackStd;
    private int defense;
    private float defenseStd;

    public Hero() {
        this.rand = new Random();
    }

    public Hero(String name) {
        this.name = name;
        this.rand = new Random();

    }

    public Hero(String name, String superPower, String elementalAffinity, String elementalVulnerability, int hp, int energy, int attack, float attackVariance, int defense, float defenseStd) {
        this.rand = new Random();
        this.name = name;
        this.superPower = superPower;
        this.elementalAffinity = elementalAffinity;
        this.elementalVulnerability = elementalVulnerability;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.attackStd = attackVariance;
        this.defense = defense;
        this.defenseStd = defenseStd;
    }

    public float attack(){
        float minus=-1;
        if(this.rand.nextFloat()>0.5){
            minus=1;
        }
     double attackResult = this.rand.nextFloat()*this.attackStd*minus +this.attack;
     Double temp=new Double(attackResult);
     return temp.floatValue();
    }

    public float defense(){
        float minus=-1;
        if(this.rand.nextFloat()>0.5){
            minus=1;
        }
        double defenseResult = this.rand.nextFloat()*this.defenseStd*minus +this.defense;
        Double temp=new Double(defenseResult);
        return temp.floatValue();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuperPower() {
        return superPower;
    }

    public void setSuperPower(String superPower) {
        this.superPower = superPower;
    }

    public String getElementalAffinity() {
        return elementalAffinity;
    }

    public void setElementalAffinity(String elementalAffinity) {
        this.elementalAffinity = elementalAffinity;
    }

    public String getElementalVulnerability() {
        return elementalVulnerability;
    }

    public void setElementalVulnerability(String elementalVulnerability) {
        this.elementalVulnerability = elementalVulnerability;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        if(Const.HP_BOUND_MAX<hp){
            this.hp=Const.HP_BOUND_MAX;
            return;
        }

        if(Const.HP_BOUND_MIN>hp) {
            this.hp = Const.HP_BOUND_MIN;
            return;
        }

        this.hp = hp;
    }

    public int getEnergy() {

        return energy;
    }

    public void setEnergy(int energy) {
        if(Const.ENERGY_BOUND_MAX<energy){
            this.energy=Const.ENERGY_BOUND_MAX;
            return;
        }

        if(Const.ENERGY_BOUND_MIN>energy) {
            this.energy = Const.ENERGY_BOUND_MIN;
            return;
        }
        this.energy = energy;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        if(Const.ATTACK_BOUND_MAX<attack){
            this.attack=Const.ATTACK_BOUND_MAX;
            return;
        }

        if(Const.ATTACK_BOUND_MIN>attack) {
            this.attack = Const.ATTACK_BOUND_MIN;
            return;
        }
        this.attack = attack;
    }

    public float getAttackStd() {
        return attackStd;
    }

    public void setAttackStd(float attackStd) {
        this.attackStd = attackStd;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        if(Const.DEFENSE_BOUND_MAX<defense){
            this.defense=Const.DEFENSE_BOUND_MAX;
            return;
        }

        if(Const.DEFENSE_BOUND_MIN>defense) {
            this.defense = Const.DEFENSE_BOUND_MIN;
            return;
        }
        this.defense = defense;
    }

    public float getDefenseStd() {
        return defenseStd;
    }

    public void setDefenseStd(float defenseStd) {
        this.defenseStd = defenseStd;
    }


    // form simple json requirements
    public String toJson() {
        final StringWriter writable = new StringWriter();
        try {
            this.toJson(writable);
        } catch (final IOException e) {
        }
        return writable.toString();
    }

    public void toJson(Writer writer) throws IOException {
        final JsonObject json = new JsonObject();
        json.put("name", this.getName());
        json.put("superPower", this.getSuperPower());
        json.put("elementalAffinity", this.getElementalAffinity());
        json.put("elementalVulnerability", this.getElementalVulnerability());
        json.put("hp", this.getHp());
        json.put("energy", this.getEnergy());
        json.put("attack", this.getAttack());
        json.put("attackStd", this.getAttackStd());
        json.put("defense", this.getDefense());
        json.put("defenseStd", this.getDefenseStd());
        json.toJson(writer);
    }

    @Override
    public boolean equals(Object obj) {
        boolean isSame=true;
        if(obj instanceof Hero){
            Hero otherHero=(Hero)obj;
            if(! otherHero.getName().equals(getName())){
                isSame=false;
            }
            if(! otherHero.getElementalAffinity().equals(getElementalAffinity())){
                isSame=false;
            }
            if(! otherHero.getElementalVulnerability().equals(getElementalVulnerability())){
                isSame=false;
            }
            if( otherHero.getAttack()!= getAttack()){
                isSame=false;
            }
            if( otherHero.getDefense()!= getDefense()){
                isSame=false;
            }
            if( otherHero.getAttackStd()!= getAttackStd()){
                isSame=false;
            }
            if( otherHero.getDefenseStd()!= getDefenseStd()){
                isSame=false;
            }
            if( otherHero.getEnergy()!= getEnergy()){
                isSame=false;
            }
            if( otherHero.getHp()!= getHp()){
                isSame=false;
            }
            return isSame;
        }else{
            return false;
        }
    }
}
