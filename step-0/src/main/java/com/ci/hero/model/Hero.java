package com.ci.hero.model;

import java.util.Random;

public class Hero {
    private Random rand;
    private String name;
    private String superPower;
    private String elementalAffinity;
    private String elementalVulnerability;
    private int hp;
    private int energy;
    private int attack;
    private float attackStd;
    private int defense;
    private float defenseStd;

    public Hero(String name) {
        this.name = name;
        this.rand = new Random();

    }

    public Hero(String name, String superPower, String elementalAffinity, String elementalVulnerability, int hp, int energy, int attack, float attackVariance, int defense, float defenseStd) {
        this.rand = new Random();
        this.name = name;
        this.superPower = superPower;
        this.elementalAffinity = elementalAffinity;
        this.elementalVulnerability = elementalVulnerability;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.attackStd = attackVariance;
        this.defense = defense;
        this.defenseStd = defenseStd;
    }

    public float attack(){
     double attackResult = this.rand.nextGaussian()*this.attackStd +this.attack;
     Double temp=new Double(attackResult);
     return temp.floatValue();
    }

    public float defense(){
        double defenseResult = this.rand.nextGaussian()*this.defenseStd +this.defense;
        Double temp=new Double(defenseResult);
        return temp.floatValue();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuperPower() {
        return superPower;
    }

    public void setSuperPower(String superPower) {
        this.superPower = superPower;
    }

    public String getElementalAffinity() {
        return elementalAffinity;
    }

    public void setElementalAffinity(String elementalAffinity) {
        this.elementalAffinity = elementalAffinity;
    }

    public String getElementalVulnerability() {
        return elementalVulnerability;
    }

    public void setElementalVulnerability(String elementalVulnerability) {
        this.elementalVulnerability = elementalVulnerability;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public float getAttackStd() {
        return attackStd;
    }

    public void setAttackStd(float attackStd) {
        this.attackStd = attackStd;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public float getDefenseStd() {
        return defenseStd;
    }

    public void setDefenseStd(float defenseStd) {
        this.defenseStd = defenseStd;
    }
}
